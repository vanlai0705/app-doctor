import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from "@coreui/react";
import { UserOutlined } from "@ant-design/icons";
import CIcon from "@coreui/icons-react";

// sidebar nav config
import navigation from "./_nav";
import { Avatar, Row, Col } from "antd";

const Sidebar = () => {
  const dispatch = useDispatch();
  const show = useSelector((state) => state.sidebarShow);

  return (
    <CSidebar
      show={show}
      onShowChange={(val) => dispatch({ type: "set", sidebarShow: val })}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        <img
          className="c-avatar-img logo"
          src={"avatars/Logo.png"}
          alt=""
        />
        <CIcon
          className="c-sidebar-brand-minimized"
          name="sygnet"
          height={35}
        />
      </CSidebarBrand>
      <CSidebarNav>
        <Row justify="center" align="middle" className="mt-3 mb-3">
          <Col span={12} className="d-flex justify-content-center">
            <Avatar
              style={{ backgroundColor: "#FFC329" }}
              icon={<UserOutlined />}
              size={64}
            />
          </Col>
          <Col span={12} className="d-flex align-items-center">
            <span style={{ color: "white", fontSize: 20 }}>홍길동 님</span>
          </Col>
        </Row>
        <Row
          className="box d-flex justify-content-center m-2 p-3"
        >
          <Col span={24} className="d-flex justify-content-center">
            <span>HTML 퍼블리싱</span>
          </Col>
          <Col span={24} className="d-flex justify-content-center">
              <span>빠른 견적 시스템</span>
          </Col>
        </Row>
        <CCreateElement
          items={navigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle,
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none" />
    </CSidebar>
  );
};

export default React.memo(Sidebar);
