import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon name="cil-home" customClasses="c-sidebar-nav-icon"/>,
    badge: {
      color: 'info',
    }
  },
  {
    _tag: 'CSidebarNavItem',
    name: '승인하기',
    to: '/approve',
    icon: <img src={'icons/cart.svg'} className="icon-sidebar" alt="icon"/>,
    badge: {
      color: 'info',
    }
  },
  {
    _tag: 'CSidebarNavItem',
    name: '서비스구매',
    to: '/service',
    icon: <img src={'icons/note.png'} className="icon-sidebar" alt="icon"/>,
    badge: {
      color: 'info',
    }
  },
  
]

export default _nav
