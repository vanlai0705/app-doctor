import instance from "./index";

export const GET = (endpoint) => {
  return instance.get(endpoint);
};

export const POST = (endpoint, body) => {
  return instance.post(endpoint, body);
};

export const DELETE = (endpoint, body) => {
  return instance.delete(endpoint, body);
};

export const PUT = (endpoint, body) => {
  return instance.put(endpoint, body);
};
