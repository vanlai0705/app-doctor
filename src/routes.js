import React from 'react';
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Approve = React.lazy(() => import('./views/Approve'));
const Cart = React.lazy(() => import('./views/Cart'));

/*
서비스구매 : Service
승인하기 : Approve
*/
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/approve', name: '승인하기', component: Approve },
  { path: '/service', name: '서비스구매', component: Cart },

];

export default routes;
