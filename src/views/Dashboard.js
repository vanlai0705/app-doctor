/* eslint-disable jsx-a11y/alt-text */
import {
  Card,
  Row,
  Col,
  Progress,
  Space,
  Steps,
  Popover,
  Tabs,
  Divider,
  Button,
} from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";

const { TabPane } = Tabs;
const { Step } = Steps;
const data = [];

for (let i = 0; i < 5; i++) {
  let id = i + 1;
  data.push({
    id: id,
    number: 34,
    name: "안드로이드 소스 분석",
  });
}
const Dotsm = styled.div`
  height: 10px;
  width: 10px;
  background-color: ${(props) => props.color};
  border-radius: 50%;
  margin-left: 2px;
`;
const Dots = styled.div`
  height: 21px;
  width: 21px;
  background-color: ${(props) => props.color};
  border-radius: 50%;
  display: flex;
  position: absolute;
  top: -6px;
`;
const BoxIssue = styled.div`
  border-width: 1px;
  background-color: ${(props) => props.color};
  padding: 5px;
  text-align: center;
  color: white;
  font-weight: 600;
`;
const Text = styled.span`
  font-size: 14px;
  color: ${(props) => props.color};
  font-weight: ${(props) => props.fontWeight};
 
`;

const ICard = styled(Card)`
.ant-card-body {
  @media (min-width: 375px) and (max-width: 1023px) {
    background-color:none

  }
  @media (min-width: 1024px) {
    background-color:#EBEDEF

  }
}
`
function Dashboard  ()  {
  const [width, setWidth] = useState(window.innerWidth);
  const [dataRequire] = useState([
    {
      id: 1,
      name: " 저희가 사용중인 앱을 수정하고자 하는데.",
      time: "2020.01.13",
      action: "담당자 확인중",
    },
    {
      id: 2,
      name: "앱닥터 수정사항 요청하고 싶습니다. 미팅도 가능한가요?",
      time: "2020.01.10",
      action: "답변 완료 ",
    },
    {
      id: 3,
      name: "앱닥터 서비스 어떻게 이용하는 건가요?",
      time: "2020.01.01",
      action: "답변 완료",
    },
  ]);
  const updateWidthAndHeight = () => {
    setWidth(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener("resize", updateWidthAndHeight);
    return () => window.removeEventListener("resize", updateWidthAndHeight);
  }, [width]);

  const customDot = (dot, { status, index, title }) => (
    <Popover
      content={
        <span>
          step {index} status: {status}
        </span>
      }
    >
      <Dots color={renderColorDot(index)} />
    </Popover>
  );
  const renderColorDot = (index) => {
    if (index > 3 && index < 7) {
      return "#6E88A6";
    } else if (index === 7) {
      return "green";
    } else {
      return "#58C8C5";
    }
  };
  const dispatch = useDispatch()

  useEffect(()=>{
    dispatch({type: 'title', title: "대시보드"})
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])
  return (
    <>
      {width < 576 && (
        <Row align="middle" justify="center" className="mb-4">
          <Col span={12} className="box-border-left bg-secondary">
            <div className="flex-center">
              <span className="text-white fs-18 font-600">
                서비스 진행 현황
              </span>
            </div>
          </Col>
        </Row>
      )}
      <Row>
        <Col span={24}>
          <Card
            style={{
              borderBottomRightRadius: 10,
              borderTopRightRadius: 10,
              margin: width < 576 ? "0px 10px" : 0,
            }}
            bodyStyle={{
              padding: 0,
            }}
            className="card-infor"
          >
            <Row>
              {width > 576 && (
                <Col
                  lg={3}
                  xs={24}
                  className="box-border-left bg-secondary"
                  style={{
                    alignItems: "center",
                    display: "flex",
                    justifyContent: "center",
                    padding: 20,
                  }}
                >
                  <div className="flex-center">
                    <span className="text-white fs-18 font-600">
                      서비스 진행 현황
                    </span>
                  </div>
                </Col>
              )}

              <Col lg={21} xs={24} style={{ padding: 20 }}>
                <Row>
                  <Col
                    span={8}
                    style={{
                      alignItems: "center",
                      display: "flex",
                      justifyContent: "center",
                      flexDirection: "column",
                    }}
                  >
                    <div className="flex-center">
                      <span className="text-black font-600">진행중</span>
                    </div>
                    <div className="flex-center">
                      <span className="text-time">
                        28 <span className="fs-18">시간</span>
                      </span>
                    </div>
                    <div className="flex-center">
                      <Progress
                        strokeColor="#446698"
                        percent={28}
                        showInfo={false}
                      />
                    </div>
                  </Col>
                  <Col
                    span={8}
                    style={{
                      alignItems: "center",
                      display: "flex",
                      justifyContent: "center",
                      flexDirection: "column",
                    }}
                  >
                    <div className="flex-center">
                      <span className="text-black font-600">전체완료</span>
                    </div>
                    <div>
                      <span className="text-timefinish">
                        456 <span className="fs-18">시간</span>
                      </span>
                    </div>
                    <div className="flex-center">
                      <Progress
                        strokeColor="#C79C07"
                        percent={87}
                        showInfo={false}
                      />
                    </div>
                  </Col>
                  <Col
                    span={8}
                    style={{
                      alignItems: "center",
                      display: "flex",
                      justifyContent: "center",
                      flexDirection: "column",
                    }}
                  >
                    <div className="flex-center">
                      <span className="text-black font-600">잔여시간</span>
                    </div>
                    <div className="flex-center">
                      <span className="text-timeback">
                        12 <span className="fs-18">시간</span>
                      </span>
                    </div>
                    <div className="flex-center">
                      <Progress
                        strokeColor="#446698"
                        percent={28}
                        showInfo={false}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
      <Row className="mt-3 mb-3">
        <Col span={24} className="coupon ">
          쿠폰 유효기간( ~ 2020년 3월 10일)이 얼마 남지 않았습니다.
        </Col>
      </Row>
      <ICard bodyStyle={{ padding: 0 }} style={{backgroundColor:'none'}}>
        <Row className="mt-3" align="middle" justify="space-between">
          <Col md={7} xs={7} className="service-coupon rounded">
            시간제 쿠폰 서비스
          </Col>
          <Col md={7} xs={7} className="service-payment rounded">
            월 후불제
          </Col>
          <Col
            xs={7}
            md={7}
            className="service-payment rounded"
            style={{ width: "100%" }}
          >
            프로젝트
          </Col>
        </Row>
        <div className=" mt-3 mb-3 process">
          <span>앱닥터 업무 프로세스</span>
        </div>

        <Row justify="center">
          <Steps current={1} progressDot={customDot}>
            <Step />
            <Step title="문의/요청" />
            <Step title="요청 분석" />
            <Step title="타원 2 복사 2" />
            <Step title="문의/요청" />
            <Step title="요청 분석" />
            <Step title="타원 2 복사 2" />
            <Step title="개발자 배정" />
          </Steps>
        </Row>
      </ICard>

      <Row justify="space-between">
        <Col className="gutter-row" md={12} xs={24}>
          <Card bordered>
            <Tabs
              defaultActiveKey="2"
              tabBarExtraContent={
                <div style={{ display: "flex", justifyContent: "flex-end" }}>
                  <Button
                    style={{ backgroundColor: "#7D7D7D", color: "white" }}
                  >
                    사각형 17
                  </Button>
                </div>
              }
            >
              <TabPane tab={<span> 승인 대기 중인 이슈 5건</span>} key="1">
                {data.map((item, index) => {
                  return (
                    <div className="m-2 mt-3">
                      <Card
                        style={{
                          backgroundColor: "#F5F8FB",
                          border: "1px solid #EDEDEF",
                          borderRadius: 10,
                          paddingTop: 8,
                          paddingBottom: 8,
                        }}
                      >
                        <div
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                            display: "flex",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              flexDirection: "row",
                              display: "flex",
                              alignItems: "center",
                            }}
                          >
                            <Space>
                              <BoxIssue color="#13B5B1">
                                Issue, {item.id}
                              </BoxIssue>
                              <span>{item.name}</span>
                            </Space>
                          </div>
                          <div>
                            <Space>
                              <Divider
                                type="vertical"
                                style={{ borderColor: "#DDDDDD", height: 31 }}
                              />
                              <span>{item.number}</span>
                            </Space>
                          </div>
                        </div>
                      </Card>
                    </div>
                  );
                })}
              </TabPane>
            </Tabs>
          </Card>
        </Col>
        <Col className="gutter-row" md={12} xs={24}>
          <Card bordered>
            <Tabs defaultActiveKey="2">
              <TabPane tab={<span>진행 중인 이슈 14건 </span>} key="1">
                {data.map((item, index) => {
                  return (
                    <div className="m-2 mt-3">
                      <Card
                        style={{
                          backgroundColor: "#F5F8FB",
                          border: "1px solid #EDEDEF",
                          borderRadius: 10,
                        }}
                      >
                        <div className="flex-end fs-8 mb-1">
                          <span> 개발자 배정 </span>{" "}
                          <Dotsm color={index % 2 ? "#0DD92E" : "#B5B5B5"} />
                        </div>
                        <div
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                            display: "flex",
                            alignItems: "center",
                          }}
                        >
                          <div
                            style={{
                              flexDirection: "row",
                              display: "flex",
                              alignItems: "center",
                            }}
                          >
                            <Space>
                              <BoxIssue color="#556FB5">
                                Issue, {item.id}
                              </BoxIssue>
                              <span>{item.name}</span>
                            </Space>
                          </div>

                          <div>
                            <Space>
                              <div>2019.04.09</div>
                              <Divider
                                type="vertical"
                                style={{ borderColor: "#DDDDDD", height: 31 }}
                              />
                              <span>{item.number}</span>
                            </Space>
                          </div>
                        </div>
                      </Card>
                    </div>
                  );
                })}
              </TabPane>
            </Tabs>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col className="gutter-row" span={24}>
          <Card bordered>
            <Tabs
              defaultActiveKey="2"
              tabBarExtraContent={
                  <div className="align-items-center d-flex">
                    <span>더보기</span>{" "}
                    <img
                      src="https://www.biogasworld.com/wp-includes/css/Font_Awesome/fontawesome-free-5.1.0-web/svgs/solid/arrow-circle-right.svg"
                      style={{ width: 15, height: 15 }}
                    />
                  </div>
              }
            >
              <TabPane tab={<span> 최근 문의 / 요청 현황</span>} key="1">
                {dataRequire.map((item, idx) => {
                  return (
                    <div className="mt-3">
                      <Text color="#959595" fontWeight="500" className="d-block d-sm-none">
                        {item.time}
                      </Text>

                      <Row className="fs-14">
                        <Col md={18}>
                          <span className="font-600">{item.name}</span>
                        </Col>
                        <Col md={6}>
                          <div className="d-flex justify-content-between">
                            <Text color="#959595" fontWeight="500" className="d-none d-sm-block">
                              {item.time}
                            </Text>
                            <Text
                              color={
                                item.action === "담당자 확인중" && "#32BAB6"
                              }
                              fontWeight="600"
                              className="float-right"
                            >
                              {item.action}
                            </Text>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  );
                })}
              </TabPane>
            </Tabs>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col className="gutter-row" span={24}>
          <Card bordered>
            <Tabs defaultActiveKey="2">
              <TabPane tab={<span>앱닥터 소식</span>} key="1">
                {[{}].map((item, idx) => {
                  return (
                    <div>
                      <Row>
                        <Col lg={12} md={24} sm={24} className="mb-2">
                          <Row align="middle">
                            <Col lg={8} md={5} sm={5} xs={8}>
                              <img
                                src={"avatars/image1.png"}
                                className="image-new"
                              />
                            </Col>
                            <Col lg={16} md={19} sm={19} xs={16}>
                              <div className="infor-new">
                                앱닥터 허석균 대표, 김빛나 본부장
                                [아산나눔재단/MARU180] 인터뷰 소식!!
                              </div>
                              <div className={width <576 ?"text-truncate":"content"}>
                                안녕하세요. 앱닥터 입니다. 오늘 알려드릴 소식은
                                아산나눔재단/MARU180에서 진행한 인터뷰
                                소식입니다. 이번 인터뷰의 주인공은 바로 앱닥터의
                                허석균 대표와 김빛나 본부장입니다. MARU180의 새
                                식구가 된 것을 격하게 환영해 주셔서 진심으로
                                감사한 자리였는데…
                              </div>
                              <div className="align-items-center d-flex justify-content-end">
                                    <span>더보기</span>{" "}
                                    <img
                                      src="https://www.biogasworld.com/wp-includes/css/Font_Awesome/fontawesome-free-5.1.0-web/svgs/solid/arrow-circle-right.svg"
                                      style={{ width: 15, height: 15 }}
                                    />
                                  </div>
                            </Col>
                          </Row>
                        </Col>
                        <Col lg={{ span: 11, offset: 1 }} md={24} xs={24}>
                          <Row>
                            <Col lg={24} md={24} xs={24}>
                              <Row align="middle">
                                <Col lg={5} md={5} xs={8} sm={5} >
                                  <img
                                    src={"avatars/image2.png"}
                                    style={{ width: 100, height: 100 }}
                                  />
                                </Col>
                                <Col lg={19} md={19} xs={16} sm={19}>
                                  <div className="infor-new">
                                    앱닥터, 앱/웹 유지 보수 재계약 안하신
                                    대표님들 주목~!
                                  </div>
                                  <div className={width <576 ?"text-truncate":"content"}>
                                    “텍스트 하나만 수정하고 싶은데”, “이미지
                                    하나 더 추가하고 싶은데” “텍스트 하나만
                                    수정하고 싶은데”, “이미
                                  </div>
                                 
                                </Col>
                              </Row>
                            </Col>
                            <Col lg={24} md={24} xs={24} className="mt-2">
                              <Row align="middle">
                                <Col  lg={5} md={5} xs={8} sm={5}>
                                  <img
                                    src={"avatars/image2.png"}
                                    style={{ width: 100, height: 100 }}
                                  />
                                </Col>
                                <Col lg={19} md={19} xs={16} sm={19}>
                                  <div className="infor-new">
                                    앱닥터, 앱/웹 유지 보수 재계약 안하신
                                    대표님들 주목~!
                                  </div>
                                  <div className={width <576 ?"text-truncate":"content"}>
                                    “텍스트 하나만 수정하고 싶은데”, “이미지
                                    하나 더 추가하고 싶은데” “텍스트 하나만
                                    수정하고 싶은데”, “이미
                                  </div>
                                </Col>
                              </Row>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </div>
                  );
                })}
              </TabPane>
            </Tabs>
          </Card>
        </Col>
      </Row>
    </>
  );
};

export default Dashboard;
