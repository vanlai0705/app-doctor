/* eslint-disable jsx-a11y/alt-text */
import { img } from "@coreui/react";
import {
  Card,
  Row,
  Col,
  Tabs,
  Divider,
} from "antd";
import React, { useState } from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";

const { TabPane } = Tabs;
const data = [];

for (let i = 0; i < 5; i++) {
  let id = i + 1;
  data.push({
    id: id,
    number: 34,
    name: "안드로이드 소스 분석",
  });
}

//승인하기 : Cart
function Cart  ()  {
  const [data] = useState([
    {
      id: 1,
      title: "개발자 시간제 쿠폰",
      name: "20시간 쿠폰 ",
      oldprice: "700,000원",
      newprice: "650,000원 ",
    },
    {
      id: 2,
      title: "개발자 시간제 쿠폰",
      name: "40시간 쿠폰 ",
      oldprice: "1,200,000원",
      newprice: "1,100,000원 ",
    },
  ]);
  const dispatch = useDispatch()

  useEffect(()=>{
    dispatch({type: 'title', title: "서비스 구매"})
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])
  return (
    <>
      <div>
        <div className="cart">
          <Card
            title={<h2 className="font-weight-bold pt-4 pb-2 d-none d-sm-block">서비스 구매</h2>}>
            <Tabs defaultActiveKey="1">
              <TabPane tab="추천" key="1"         style={{ color: "#363636" }}
>
                <Row>
                  <Col span={24} className="d-flex justify-content-center">
                    <img src={"avatars/banner.png"} className="banner" />
                  </Col>
                </Row>
                <Divider />
                {data.map((item, idx) => {
                  return (
                    <div>
                      <Row
                        align="middle"
                        className="space-voucher"
                        key={idx}
                        justify="center">
                        <Col md={8} sm={24}>
                          <div>
                            <img
                              src={"avatars/voucher.png"}
                              className="voucher-img"
                            />
                            <div class="bottom-left">
                              <div className="voucher-title">
                                <span>{item.title}</span>
                              </div>
                              <span className="font-weight-bold ml-3">
                                {item.name}
                              </span>
                            </div>
                          </div>
                        </Col>
                        <Col md={16} sm={24} className="d-none d-sm-block">
                          <Row align="middle" justify="center">
                            <Col sm={8}>
                              <span className="old-price">{item.oldprice}</span>
                            </Col>
                            <Col sm={8}>
                              <span className="new-price">{item.newprice}</span>
                            </Col>
                            <Col sm={8}>
                              <div className="button-buy">구매하기</div>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                      <div className="d-block d-sm-none d-flex justify-content-between align-items-center mt-3 mb-3">
                        <span className="old-price">{item.oldprice}</span>
                        <span className="new-price">{item.newprice}</span>
                        <div className="button-buy">구매하기</div>
                      </div>
                    </div>
                  );
                })}
              </TabPane>
              <TabPane tab="시간제 쿠폰" key="2">
              시간제 쿠폰
              </TabPane>
            </Tabs>
          </Card>
        </div>
      </div>
    </>
  );
};

export default Cart;
